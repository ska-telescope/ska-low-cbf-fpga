PROJECT = ska-low-cbf-fpga

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .
CI_PROJECT_PATH_SLUG ?= ska-low-cbf-fpga
CI_ENVIRONMENT_SLUG ?= ska-low-cbf-fpga

# define private overrides for above variables in here
-include PrivateRules.mak

# Include the required modules from the SKA makefile submodule
include .make/base.mk
include .make/python.mk

PYTHON_LINE_LENGTH = 88
PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=src:src/ska_low_cbf_fpga  poetry run
PYTHON_BUILD_TYPE = non_tag_setup

python-pre-test:
	poetry install -E console --no-interaction

# workaround for bug in CI templates (copied from https://gitlab.com/ska-telescope/low-cbf/ska-low-cbf-proc)
python-pre-publish:
	pip3 install twine
