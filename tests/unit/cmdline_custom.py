"""
Derived FpgaCmdline for testing downstream customisation features
"""
from ska_low_cbf_fpga.fpga_cmdline import FpgaCmdline


class Custom:
    def __init__(*args, **kwargs):
        pass


cmdline = FpgaCmdline(personality_map={"test": Custom})

# ensure that our specified class was actually used
for device, fpga in cmdline.fpgas.items():
    assert isinstance(fpga, Custom)
