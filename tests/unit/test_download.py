# -*- coding: utf-8 -*-
#
# Copyright (c) 2024 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence Agreement
# See LICENSE.txt for more info.


from ska_low_cbf_fpga.download import NexusFileFetcher

VERSION = "0.0.11"  # FW version we are trying to "download"

PACKAGES_A = [
    {
        "name": "ska-low-cbf-fw-corr-u55c-0.0.118.tar.gz",
        "url": "https://artefact.skao.int/repository/raw-internal/ska-low-cbf-fw-corr-u55c-0.0.118.tar.gz",
        "csum": "7babf6ee2c99823397cdc8b1850060d5a5758d32",
    },
    {
        "name": "ska-low-cbf-fw-corr-u55c-0.0.119.tar.gz",
        "url": "https://artefact.skao.int/repository/raw-internal/ska-low-cbf-fw-corr-u55c-0.0.119.tar.gz",
        "csum": "babef6ee2c99823397cdc8b1850060d5a5758d32",
    },
]

PACKAGES_B = [
    {
        "name": "ska-low-cbf-fw-corr-u55c-0.0.118.tar.gz",
        "url": "https://artefact.skao.int/repository/raw-internal/ska-low-cbf-fw-corr-u55c-0.0.118.tar.gz",
        "csum": "7babf6ee2c99823397cdc8b1850060d5a5758d32",
    },
    {
        "name": "ska-low-cbf-fw-corr-u55c-0.0.11.tar.gz",
        "url": "https://artefact.skao.int/repository/raw-internal/ska-low-cbf-fw-corr-u55c-0.0.11.tar.gz",
        "csum": "don't care",
    },
    {
        "name": "ska-low-cbf-fw-corr-u55c-0.0.119.tar.gz",
        "url": "https://artefact.skao.int/repository/raw-internal/ska-low-cbf-fw-corr-u55c-0.0.119.tar.gz",
        "csum": "babef6ee2c99823397cdc8b1850060d5a5758d32",
    },
    {
        "name": "ska-low-cbf-fw-corr-u55c-0.0.111.tar.gz",
        "url": "https://artefact.skao.int/repository/raw-internal/ska-low-cbf-fw-corr-u55c-0.0.111.tar.gz",
        "csum": "don't care",
    },
]


def test_find_no_match():
    "Confirm there is no exact match among similar versions"
    exact_match, index = NexusFileFetcher.find_exact_match(PACKAGES_A, VERSION)
    assert not exact_match
    assert index == 0


def test_find_exact_match():
    "Confirm the exact version is picked among similar looking versions"
    exact_match, index = NexusFileFetcher.find_exact_match(PACKAGES_B, VERSION)
    assert exact_match
    assert index == 1
