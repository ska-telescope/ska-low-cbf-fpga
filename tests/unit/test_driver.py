# -*- coding: utf-8 -*-
#
# Copyright (c) 2022 CSIRO Space and Astronomy.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
"""Tests for driver.py"""
from ska_low_cbf_fpga import ArgsFpgaDriver, ArgsMap, create_driver_map_info


class TestCreateDriverMapInfo:
    """
    Test the create_driver_map_info factory function
    """

    def test_simulation_no_hardware(self, simulator_map_path):
        """FPGA map provided, no FPGA hardware"""
        # TODO - if/when we get FPGA-in-the-loop testing, will need some
        #  way to control when this test is run... for now we assume no
        #  FPGA hardware (because there isn't any FPGA in  our CI pipeline)
        driver, map_, info = create_driver_map_info(fpgamap_path=simulator_map_path)
        assert isinstance(driver, ArgsFpgaDriver)
        assert isinstance(map_, ArgsMap)
        assert info is None
